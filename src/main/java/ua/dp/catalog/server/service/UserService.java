package ua.dp.catalog.server.service;

import ua.dp.catalog.server.model.User;

import java.util.List;

/**
 * Created by y.voytovich on 16.09.2015.
 */
public interface UserService {
    public void addUser(User user);
    public void deleteUser(Integer id);
    public void editUser(User user);
    public List<User> getAll();
    public User getUserById(Integer id);
    public User getUserByUserName(String userName);
}
