package ua.dp.catalog.server.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Transactional;
import ua.dp.catalog.server.dao.UserDao;

/**
 * Created by y.voytovich on 02.10.2015.
 */
public class AuthServiceImpl implements UserDetailsService {


    @Autowired
    UserDao userDao;

    @Transactional
    @Override
    public UserDetails loadUserByUsername(String username)
            throws UsernameNotFoundException {

        ua.dp.catalog.server.model.User details = userDao.getUserByUserName(username);
        Collection<simplegrantedauthority> authorities = new ArrayList<simplegrantedauthority>();
        SimpleGrantedAuthority userAuthority = new SimpleGrantedAuthority(
                "ROLE_USER");
        SimpleGrantedAuthority adminAuthority = new SimpleGrantedAuthority(
                "ROLE_ADMIN");
        if (details.getRole().equals("user"))
            authorities.add(userAuthority);
        else if (details.getRole().equals("admin")) {
            authorities.add(userAuthority);
            authorities.add(adminAuthority);
        }
        UserDetails user = new User(details.getUsername(),
                details.getPassword(), true, true, true, true, authorities);
        return user;
    }

}
