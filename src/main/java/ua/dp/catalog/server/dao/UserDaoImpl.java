package ua.dp.catalog.server.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ua.dp.catalog.server.model.User;

import java.util.List;

/**
 * Created by y.voytovich on 16.09.2015.
 */
@Repository
@Transactional
public class UserDaoImpl implements UserDao {

    Logger logger = LoggerFactory.getLogger(UserDaoImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void addUser(User user) {
        sessionFactory.getCurrentSession().persist(user);

    }

    @Override
    public void deleteUser(Integer id) {
        Session session = sessionFactory.getCurrentSession();
        User user = (User) session.load(User.class, id);
        session.delete(user);
    }

    @Override
    public void editUser(User user1) {
        Session session = sessionFactory.getCurrentSession();
        User user = getUserById(user1.getId());
        user.setUsername(user1.getUsername());
        user.setPassword(user1.getPassword());
        user.setRole(user1.getRole());
        user.setEnabled(user1.getEnabled());
        session.save(user);
    }

    @Override
    public List<User> getAll() {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM User");
        return query.list();
    }

    @Override
    public User getUserById(Integer id) {
        Session session = sessionFactory.getCurrentSession();
        User user = (User) session.get(User.class, id);
        return user;
    }

    @Override
    public User getUserByUserName(String username) {
        Session session = sessionFactory.getCurrentSession();
        User user = (User) session.get(User.class, username);
        return user;
    }
}
