package ua.dp.catalog.server.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ua.dp.catalog.server.model.User;
import ua.dp.catalog.server.service.UserService;
import ua.dp.catalog.server.utils.SpringSecurityUtil;


/**
 * Created by e.krotov@hotmail.com on 17.09.2015.
 */
@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    UserService userService;

    @RequestMapping(value = "/current", method = RequestMethod.GET)
    public User get() {
        return  userService.getUserByUserName(SpringSecurityUtil.getUsername());
    }

    @RequestMapping(value = "/add/{username}/{password}", method = RequestMethod.PUT)
    public void add(@PathVariable String username, @PathVariable String password) {
        userService.addUser(new User(username, password));
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public void update(User user) {
        userService.editUser(user);
    }

    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public void delete(User user) {
        userService.deleteUser(user.getId());
    }
}
