package ua.dp.catalog.server.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ua.dp.catalog.server.model.User;
import ua.dp.catalog.server.service.UserService;

/**
 * Created by y.voytovich on 18.09.2015.
 */
@RestController
@RequestMapping("/fill")
public class FillController {


    @Autowired
    UserService userService;

    @RequestMapping(method = RequestMethod.GET)
    public void fillDB() {



            userService.addUser(new User(10, "evgen", "123", true, "ROLE_ADMIN"));
            userService.addUser(new User(11, "yuri", "123", true, "ROLE_ADMIN"));
            userService.addUser(new User(12, "dan", "123", true, "ROLE_ADMIN"));
            userService.addUser(new User(13, "mkyoung", "123", true, "ROLE_USER"));


    }



}
