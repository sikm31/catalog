package ua.dp.catalog.server.utils;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;


public final class SpringSecurityUtil {

    public static String getUsername() {
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }

}