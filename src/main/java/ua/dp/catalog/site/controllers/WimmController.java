package ua.dp.catalog.site.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by dan on 29.08.2015.
 */
@Controller
@RequestMapping("/")
public class WimmController {

    @RequestMapping(method = RequestMethod.GET)
    public String getWimmPage() {
        return "redirect:/index.html";
    }



}
